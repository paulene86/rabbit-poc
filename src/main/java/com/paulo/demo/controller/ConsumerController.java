package com.paulo.demo.controller;


import com.paulo.demo.service.ConsumerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
public class ConsumerController {

    private ConsumerService consumerService;

    public ConsumerController(ConsumerService consumerService){
        this.consumerService=consumerService;
    }


    @GetMapping("/consume")
    public  String consumerMessage() throws IOException, TimeoutException {
        return consumerService.consumeMessage();
    }


}

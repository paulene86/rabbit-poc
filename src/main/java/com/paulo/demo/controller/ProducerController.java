package com.paulo.demo.controller;

import business.DataStreamIn;
import business.Response;
import com.paulo.demo.service.ProducerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
public class ProducerController {

    private ProducerService producerService;

    public ProducerController(ProducerService producerService){
        this.producerService=producerService;
    }


    @PostMapping("/publish")
    public Response publishMessage(@RequestBody DataStreamIn dataStreamIn)  throws IOException, TimeoutException {
        return producerService.publishMessage(dataStreamIn);
    }


}

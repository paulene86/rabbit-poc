package com.paulo.demo.service;

import business.DataStreamIn;
import business.Response;
import org.springframework.stereotype.Service;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


@Service
public class ProducerServiceImpl implements ProducerService{

    private static final  String QUEUE_NAME= "firts_queue";

    @Override
    public Response publishMessage(DataStreamIn dataStreamIn) throws IOException, TimeoutException  {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()){
            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            channel.basicPublish("",QUEUE_NAME,null,dataStreamIn.getData().getBytes());
            System.out.println("[X] Mensaje publicado : " +  dataStreamIn.getData());
            Response response = new Response();
            response.setValue("Mensaje publicado");
            return response;
        }






    }





    }


package com.paulo.demo.service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface ConsumerService {

    String consumeMessage() throws IOException, TimeoutException;


}

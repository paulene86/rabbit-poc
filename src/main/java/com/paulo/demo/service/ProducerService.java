package com.paulo.demo.service;

import business.DataStreamIn;
import business.Response;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface ProducerService {

    Response publishMessage(DataStreamIn dataStreamIn) throws IOException, TimeoutException;
}

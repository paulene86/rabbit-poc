package business;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataStreamIn {

    private String data;

}
